// 
// Author - Jack Hebert (jhebert@cs.washington.edu) 
// Copyright 2007 
// Distributed under GPLv3 
// 
// Modified - Dino Konstantopoulos
// Distributed under the "If it works, remolded by Dino Konstantopoulos, 
// otherwise no idea who did! And by the way, you're free to do whatever 
// you want to with it" dinolicense
// 
package SpeciesLabBuilder;

import Utils.ConvertXmlToMap;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.util.*; 
import java.lang.StringBuilder; 
  
 /* 
  * This class reads in a serialized download of wikispecies, extracts out the links, and 
  * foreach link: 
  *   emits (currPage, (linkedPage, 1)) 
  * 
  * 
  */ 
 public class SpeciesGraphBuilderMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> { 
  
  
   public void map(LongWritable key, Text value, 
                   OutputCollector output, Reporter reporter) throws IOException
{
     // Prepare the input data. 
    ArrayList<String> attributes = new ArrayList<>();
    GetOutlinks(value.toString(),attributes);
   } 
  
   public String GetTitle(String page, Reporter reporter) throws IOException{ 
            
            
            int start = page.indexOf("<title>");
            int end = 0;
            String title = null;
            start = start+7; 
            end = page.indexOf("</title>", start); 
              //if((end==-1)||(end-start<0)) 
              
  
            title = page.substring(start); 
            title = title.substring(0, end-start); 
            //title = title.replace(":","_");
            if(title.contains(":"))
                return "";
         
            return title;
   } 
  
   public ArrayList<String> GetOutlinks(String page,ArrayList<String> outlinks){ 
     int end; 
      
     int start=page.indexOf("\""); 
     while (start > 0) { 
       start = start+1; 
       end = page.indexOf("\"", start); 
//       if(page.indexOf("|") >= 0){
//           end = page.indexOf("|");
//       }
           
       //if((end==-1)||(end-start<0)) 
       if (end == -1) { 
         break; 
       } 
  
       String toAdd = page.substring(start,end);
         System.err.println(toAdd);
       toAdd = toAdd.substring(0,(end-start));
//       if(toAdd.contains("|")){
//           end = page.indexOf("|", start);
//           toAdd = toAdd.substring(0,end-start);
//       }
//       if(!toAdd.contains(":")){
//          outlinks.add(toAdd); 
      // }
       start = page.indexOf("\"", end+1); 
     } 
     return outlinks; 
   } 

    
 }

