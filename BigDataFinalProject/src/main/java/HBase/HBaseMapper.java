/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HBase;

/**
 *
 * @author Krunal
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
  
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
  
public class HBaseMapper extends
        Mapper<LongWritable, Text, ImmutableBytesWritable, KeyValue> {
    final static byte[] COL_FAMILY = "TagsFamily".getBytes();
  
    List<String> columnList = new ArrayList<String>();
  //  ParseXml parseXml = new ParseXml();
    ImmutableBytesWritable hKey = new ImmutableBytesWritable();
    KeyValue kv;
  
    protected void setup(Context context) throws IOException,
            InterruptedException {
        columnList.add("id");
        columnList.add("author");
        columnList.add("title");
        columnList.add("genre");
        columnList.add("price");
        columnList.add("publish_date");
        columnList.add("description");
    }
  
    /**
     * Map method gets XML data from tag <book> to </book>. To read the xml content the data is sent to getXmlTags method
     * which parse the XML using STAX parser and returns an String array of contents.
     * String array is iterated and each elements are stored in KeyValue
     * 
     */
    public void map(LongWritable key, Text value, Context context)
            throws InterruptedException, IOException {
        String line = value.toString();
  
        //String fields[] = parseXml.getXmlTags(line, columnList);
        ArrayList<Tags> attributeList = new ArrayList<>(); 
        //String fields[] = GetTagsAtribute(line, attributeList);
        for(Tags tag : attributeList){
            //hKey.set(fields[0].getBytes());
  
        if (!tag.getId().equals("")) {
            kv = new KeyValue(hKey.get(), COL_FAMILY,
                    HColumnEnum.TAG_COL_ID.getColumnName(),
                    tag.getId().getBytes());
            context.write(hKey, kv);
        }
  
        if (!tag.getTagName().equals("")) {
            kv = new KeyValue(hKey.get(), COL_FAMILY,
                    HColumnEnum.TAG_COL_TagName.getColumnName(), tag.getTagName().getBytes());
            context.write(hKey, kv);
        }
  
        if (!tag.getCount().equals("")) {
            kv = new KeyValue(hKey.get(), COL_FAMILY,
                    HColumnEnum.TAG_COL_Count.getColumnName(), tag.getCount().getBytes());
            context.write(hKey, kv);
        }
        
  
            context.write(hKey, kv);
        
        }
        
    }
    
    public ArrayList<Tags> GetTagsAtribute(String page,ArrayList<Tags> attributeList){ 
     int end; 
     int count = 0;
     
     int start=page.indexOf("\""); 
     while (start > 0) {
       
       start = start+1; 
       end = page.indexOf("\"", start); 

       if (end == -1) { 
         break; 
       } 
  
       Tags tag = new Tags();
       String toAdd = page.substring(start,end);
       //System.err.println(toAdd);
       if(count == 0){
           toAdd = toAdd.substring(0,(end-start));
           tag.setId(toAdd);
       }else if(count == 1){
           toAdd = toAdd.substring(0,(end-start));
           tag.setTagName(toAdd);
       }else if(count == 2){
           toAdd = toAdd.substring(0,(end-start));
           tag.setCount(toAdd);
       }

       attributeList.add(tag); 
       count++;
       start = page.indexOf("\"", end+1); 
     } 
     return attributeList; 
   } 
}
