/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HBase;

/**
 *
 * @author Krunal
 */
public enum HColumnEnum {
    
  TAG_COL_ID("Id".getBytes()),
  TAG_COL_TagName ("TagName".getBytes()),
  TAG_COL_Count("Count".getBytes());
  
 
  private final byte[] columnName;
  
  HColumnEnum (byte[] column) {
    this.columnName = column;
  }

  public byte[] getColumnName() {
    return this.columnName;
  }
}
