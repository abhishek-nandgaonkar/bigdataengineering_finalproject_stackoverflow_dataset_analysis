package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Main {

	HashMap<Integer,String> csvValues = new HashMap();
	File file = new File("output.csv");


	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		Main obj = new Main();
		obj.run();
	}
	
	public void run() throws IOException {
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		String csvFile = "postlinks.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {

			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String content = "This is the content to write into file";

				

				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}

		
				//bw.write(content);
				//bw.close();

				System.out.println("Done");

			        // use comma as separator
				String[] country = line.split(cvsSplitBy);
				int key = Integer.parseInt(country[0]);
				if(csvValues.containsKey(key)){
					String value = csvValues.get(key);
					String val = value + " - [[" + country[1] +"]]";
					csvValues.put(key, val);
				}
				else
				{
					csvValues.put(key, "[[" + country[1] +"]]");
				}
				//System.out.println(country[0]+ "[[" + country[1] + "]]");
				
			}
			
			Iterator it = csvValues.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		      String outputVal = (pair.getKey() + "," + pair.getValue());
		      bw.write(outputVal);
		      bw.write("\n");
		        it.remove(); // avoids a ConcurrentModificationException
		    }
			

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println("Done");
	  }

}
