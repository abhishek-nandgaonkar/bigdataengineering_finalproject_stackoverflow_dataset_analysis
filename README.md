Introduction :
Stack Exchange is a network of question and answer Web sites on topics in varied fields, each site covering a specific topic, where questions, answers, and users are subject to a reputation award process

Goal :
We need to maintain instructive information on all site to provide user with useful information.
Hence useful information can be achieved by below things.
Identify expert users depending upon different factor like age, upvote, downvote, reputation
And assigning them moderator access to maintain reliable data
Identify spammers who inject dirty data and useful information in lost or identifying users who are answering their own question and generating reputation.

Dataset :
Ask Ubuntu
Stack Overflow

Data for six years processed
Earliest Post: 2009-01-08
Latest Post: 2016-03-06

Technologies used :
Pig
HBase
Map Reduce
Hive
Apache Mahout
Azure ML Clustering
Power-BI
Amazon EC2
Java

Description:
Processsed data that spanned six years from 2009-01-08 to 2016-03-06
Analyzed data to extract meaningful statistics about the website, users and the posts
Identified expert users, spammers, new users depending on various parameters assigned to each profile
Implemented a fully distributed Hadoop cluster of 10 machines on Amazon EC2
Implemented Pig scripts to perform ETL on dataset and stored data in HBase
Queried the data store in Hbase using Hive scripts for analysis
Implemented Mahout recommendation algorithm to recommend questions to users that have answered similar questions
Implemented Azure Machine Learning clustering model to cluster users and questions with similar tags for recommending users for answering questions